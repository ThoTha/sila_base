"""
Ensure that SiLAService has major feature version `1`.
Changing it affects the fully qualified identifier of this feature which is central to SiLA 2 communication.
"""
import os
import re

from lxml import etree

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", ".."))

if __name__ == '__main__':
    feature_xml = etree.parse(
        os.path.join(ROOT_DIR, "feature_definitions", "org", "silastandard", "core", "SiLAService.sila.xml")
    )
    feature_version = feature_xml.xpath(
        "/sila:Feature/@FeatureVersion", namespaces=dict(sila="http://www.sila-standard.org")
    )[0]
    if not re.fullmatch(r"1.\d+", feature_version):
        raise ValueError("SiLAService major feature version must be 1")
