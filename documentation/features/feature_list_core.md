# SiLA 2 Core Features

These are the current SiLA 2 Core features

[AuthenticationService](feature_definitions/org/silastandard/core/AuthenticationService.sila.xml)

[AuthorizationConfigurationService](feature_definitions/org/silastandard/core/AuthorizationConfigurationService.sila.xml)

[AuthorizationProviderService](feature_definitions/org/silastandard/core/AuthorizationProviderService.sila.xml)

[AuthorizationService](feature_definitions/org/silastandard/core/AuthorizationService.sila.xml)

[CancelController](feature_definitions/org/silastandard/core/commands/CancelController.sila.xml)

[ConnectionConfigurationService](feature_definitions/org/silastandard/core/ConnectionConfigurationService.sila.xml)

[ErrorRecoveryService](feature_definitions/org/silastandard/core/ErrorRecoveryService.sila.xml)

[LockController](feature_definitions/org/silastandard/core/LockController.sila.xml)

[ParameterConstraintsProvider](feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider.sila.xml)

[PauseController](feature_definitions/org/silastandard/core/commands/PauseController.sila.xml)

[SiLAService](feature_definitions/org/silastandard/core/SiLAService.sila.xml)

[SimulationController](feature_definitions/org/silastandard/core/SimulationController.sila.xml)
